// Algorithms to path finding
var PathFinder = {
    // A* (manhattan or euclidean heuristic)
    aStar: async function(heuristic='astar-euclidean') {
        const start_point = this.start_point = Grid.start_point;
        const end_point = this.end_point = Grid.end_point;
        const abs = Math.abs;
        const sqrt = Math.sqrt;
        const pow = Math.pow;
        switch(heuristic){
            case 'astar-manhattan':
                heuristic = (p1, p2) => { return abs(p1.i - p2.i) + abs(p1.j - p2.j); } 
                break;
            case 'astar-euclidean':
                heuristic = (p1, p2) => { return sqrt(pow(p1.i - p2.i, 2) + pow(p1.j - p2.j, 2)); } 
                break;
        };


        let pprefix = this.pprefix = Array(Grid.grid_height).fill().map(
            ()=>Array(Grid.grid_width).fill());
        let items = Array(Grid.grid_height).fill().map(()=>Array(Grid.grid_width).fill());

        let heap = new Heap((a, b) => { return a.f - b.f; });
        items[start_point.i][start_point.j] = {f: 0, g: 0, cell: start_point}
        heap.push(items[start_point.i][start_point.j]);

        while(!heap.empty()) {
            var {f, g, cell} = heap.pop();

            Grid.setChecked(cell.i, cell.j);
            if (cell.type == Types.end_point)
                break;

            await this.pause();
            if (Controler.getState() == States.path_building_canceled)
                break;

            for(const nearest of Grid.getNearests(cell.i, cell.j))
                if (nearest.type == Types.cur_checked || nearest.type == Types.end_point) {
                    const ii = nearest.i;
                    const jj = nearest.j;
                    const item = {f: g + 1 + heuristic(end_point, nearest),
                                g: g + 1, 
                                cell: nearest};
                    if(!items[ii][jj]) {
                        pprefix[ii][jj] = [cell.i, cell.j];
                        items[ii][jj] = item;
                        heap.push(item);
                    }
                    else if (item.g < items[ii][jj].g){
                        items[ii][jj].f = item.f;
                        items[ii][jj].g = item.g;
                        pprefix[ii][jj] = [cell.i, cell.j];
                        heap.updateItem(items[ii][jj]);
                    }
                }
        }
    },
    // Pause path finding
    pause: async function() {
        do {
            await sleep(Controler.path_finding_speed);
        } while(Controler.getState() == States.path_building_stopped)
    },
    // Check if the end_point was reached
    isSuccess: function() {
        return this.pprefix[this.end_point.i][this.end_point.j];
    },
    // Return path from start_point to end_point
    getPath: function() {
        const pprefix = this.pprefix;

        let path = []
        let cur_point = [this.end_point.i, this.end_point.j];
        path.push(cur_point);
        while(pprefix[cur_point[0]][cur_point[1]]) {
            cur_point = pprefix[cur_point[0]][cur_point[1]].slice();
            path.push(cur_point);
        }
        path.map(arr => arr = arr.reverse());
        return path.reverse();
    },
}